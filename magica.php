<?php

	/*
	
		Clase Mágica
		
		$magica = new Magica ();

		
	
	
	*/

	class Magica  {	
		
		protected $elementos = array ();
		
		
		/*
			Asignación
			$magico->clave = '1234';
		*/		
		public function __set ($clave = 'id', $valor = null) {
			$this->valor ($clave, $valor);
		}
		
		/*
			Asignación
			$magico->valor ('clave', '1234');
		*/
		public function valor ($clave, $valor) {
			$this->elementos [$clave] = $valor;
		}
		
		
		/*
			Asignación
			$magico->agregar = ($usuario, 'nombre', 'clave');
			De un $arreglo dado agrega solo los índices específicos (o todos)
		*/
		public function agregar ($arreglo = array (), ...$indices) {
			if ($indices == array())
				if (is_array($arreglo))
					$indices = array_keys($arreglo);
				
			foreach ($indices as $indice)
				if (isset ($arreglo[$indice]))
					$this-> valor ( $indice, $arreglo [$indice]);
				
		}
		
		
		
		
		
		/*
			Recuperación
			$clave = $magico->clave;
		*/		
		public function __get ($clave = 'id') {
			return $this->obtener ($clave);
		}
		
		
		/*
			Recuperación
			$clave = $magico->obtener ('clave');
		*/
		public function obtener ($clave) {
			return array_key_exists ($clave, $this->elementos) ? $this->elementos [$clave] : null;
		}
		
		/*
			Recuperación
			$datos = $magico->todo();
		*/
		public function todo () {
			return $this->elementos;
		}
		
		
		
		
		// Propiedades y valores -------------------
		
		public function propiedades () { return array_keys     ($this->atributos()); }
		
		public function valores     () { return array_values   ($this->atributos()); }
		
		public function atributos   () { return get_class_vars (get_class($this)); }
		
		
		
		// Otros -----------------------------------
		
		// echo $magico;
		public function __toString () {
			return $this->json ();
		}
		
		// $magico->json ('{nada: 'no hay nada'}');
		public function json ($cadena = null) {
			if ($cadena === null)
				return str_ireplace (',', ',' . PHP_EOL , json_encode ($this->elementos));
			
			$arreglo = json_decode ($cadena, true);
			$this->agregar($arreglo);
		}
		
		
		
		
		// $magica->funcion (1, 2, 3);
		// ==> $funcion = 'funcion'
		// ==> $argumento = [1,2,3]
		public function __call ($funcion, $argumento) {
			
		}
		
		
		public function __isset($prop) : bool {
			return isset($this->$prop);
		}
		
	}
	
	
	
	
	
	
?>
