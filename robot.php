<?php

	
	/*
	
		RoBOT Telegram
		
		15 de febrero de 2021
		
		
		
		Javier Martínez
				
	
	
		https://gitlab.com/javipc/
		
		
		
		
		
		Proyectos en los que se utiliza este programa

		Seguime 
		https://t.me/seguimebot 

		Kawaii Tokei
		https://t.me/relojbot

		Listagrama
		https://t.me/listagramabot

						
					
		

	
		
		
		
		Ejemplo:
		
			require_once("robot.php");
				
		
			$bot = new RoBot (); // el robot
			
			$bot->clave ( '1234'); // clave del robot
			
			$bot->url ('http://url.bot/bot.php'); // webhook
						
			
			
			
			
			$peticion = $bot->peticion (); // obtiene la petición del servidor (ej: comando de un usuario)
				
			$mensaje = $bot->mensaje (); // crea un mensaje 
			
			$mensaje->enviar_texto ('445666', 'hola'); // envía un mensaje a un usuario especifico
			
			
			$mensaje = $bot->mensaje ($peticion); // crea un mensaje para responder la petición
			
			$mensaje->texto = 'hola' // define el texto.
			
			$mensaje->enviar () // envía el mensaje.




			// Botones
			$botones = ['Mensaje' => '/mensaje', 'Ayuda' => '/ayuda'];	
			$mensaje->botones ($botones);
			
			$botones = ['Más información' => '/mas_info', 'Contacto' => '/contacto'];
			$mensaje->botones ($botones);			
			
			



			Métodos sencillos:
			
			enviar_texto    ($destinatario, $mensaje, $id_respuesta= null);
			enviar_coordenada ($destinatario, $latitud, $longitud, $id_respuesta=null);
			enviar_imagen     ($destinatario, $imagen, $texto = null, $id_respuesta= null);
			enviar_dato       ($destinatario, $comando, $parametros = array ());
			
			
			
			
			
			Utiliza ideas de:
			
			https://stackoverflow.com/questions/15273570/continue-processing-php-after-sending-http-response






			La clase Robot posee métodos relacionados con la conexión y la comunicación con la API.


			La clase MensajeRobot posee métodos relacionados con la API (comandos).

		
		
	*/



	require ('magica.php');
	
	
	ini_set('max_execution_time', 30);

	
	
	
	class RoBot {
			public $webhook = true;
			
			protected $robot_url    = null;
			protected $robot_clave  = null;			
			
			protected $peticion   = null;	// Perdón por estos nombres de variables.		
						
			// Solo se puede realizar un único envío webhook
			public $webhook_enviado = false;
			
			// Salida de texos y comandos.
			public $modo_prueba = false;
			
			// Almacena todos los comandos enviados.
			public $historial  = array ();




				// ROBOT INTERMEDIO SLL
				
				// Impide el envío (conexión) (Para usar con un robot intermedio o para pruebas).
				public $no_enviar  = false;

				// Solicita respuestas, para pedir respuestas o evitarlas y ahorrar tráfico en robots intermedios.
				public $recibir_respuesta = false;

				// ----------------------


			
			
			public function __construct ($clave = null) {
				$this->clave ($clave);
				if ($clave == null)
					if (defined ('robot_clave'))
						$this->clave (robot_clave);
			}
			
			
			
		
			public function clave ($clave = null) {
				if ($clave == null)
					return $this->robot_clave;
				$this->robot_clave = $clave;
				$this->robot_url = 'https://api.telegram.org/bot' . $clave . '/';
			}
			
			
		

			// ESTOS COMANDOS NO USAN HISTORIAL 
		
			// para WEBHOOK - se usa una sola vez
			public function url ($url = 'http://javierpc.atwebpages.com/robot.php', $conexiones = 1, $actualizaciones = array ('message', 'callback_query', 'channel_post', 'inline_query')) {
				$parametros ['method'] = "setWebHook";
				$parametros ['url'] = $url;
				$parametros ['max_connections'] = $conexiones;
				$parametros ['allowed_updates'] = $actualizaciones;
				$parametros ['drop_pending_updates'] = 'True';
				$this->webhook = false;
				$this->imprimir ($this->conectar ($parametros));
			}
			
			public function info () {				
				$parametros ['method'] = "getWebhookInfo";
				$this->webhook = false;
				$this->imprimir ($this->conectar ($parametros));
			}
			
			public function borrar_url () {
				$parametros ['method'] = "deleteWebhook";
				$this->webhook = false;
				$this->imprimir ($this->conectar ($parametros));
			}
		
			
			public function salir ($chat_id) {				
				$parametros ['method'] = "leaveChat";
				$parametros ['chat_id'] = $chat_id;
				$this->conectar($parametros);
			}
			

			public function id () {
				return strstr($this->clave, ':', true);
			}

			// ------------------------------




		

			public function tiempo_maximo ($valor = 10) {
				set_time_limit($valor);
				ini_set('max_execution_time', $valor);
			}
					

			
		
		
		
			// recibe los datos del servidor, la petición.
			
			// El parámetro es para desarrollo

			public function obtener_datos ($datos_prueba = null) {
				$datos = array();
				if ($this->webhook)
					$datos = json_decode (file_get_contents("php://input"), true);
							
				else
					$datos = $this->actualizacion();				
				
				if ($datos_prueba != null)
					if ($this->webhook)
						$datos = json_decode ($datos_prueba, true);
				
				
				
				// imprime en modo de pruebas
				//$this->imprimir ($datos, 'datos');
				
				if ($datos == null)
					//return $this->mensaje();
					return null;
				
					
				// si es webhook devuelve un mensaje
				if ($this->webhook) 
					return $this->nuevo_mensaje ($datos) -> simplificar();
				
				
				$peticion = array();
				// si es api devuelve un arreglo de mensajes
				if (isset ($datos ['result']))
					if (is_array ($datos ['result']))
						foreach ($datos ['result'] as $dato)
							$peticion [] = $this->nuevo_mensaje ($dato)->simplificar ();
				
				if ($peticion != array()) {
					
					$actualizacion = end ($peticion)->update_id;
					$this->actualizacion ($actualizacion +1);
				}
				
				
				return $peticion;
				
			}
			


			// crea un objeto mensaje y vuelva datos desde $objeto_json
			
			public function nuevo_mensaje ($objeto_json = array()) {				
				$mensaje = new MensajeRobot ($this);
				if ($objeto_json != null)
				
				
				$mensaje  -> agregar ((array)$objeto_json);
				return $mensaje ;
			}
			

			// #OBSOLETO
			/*
			private function crear_peticion ($datos) {
				
				$peticion = $this->mensaje ();
				
				// if (isset ($datos['update_id'])) $datos ['message'] ['update_id'] = $datos ['update_id'];
				
				
				// carga los datos originales recibidos del servidor
				$peticion->arreglo ($datos);
				
				
				// combina con datos simples				
				$peticion->simplificar ($datos);	
						
			
				return $peticion;
			}
				*/
					
			
			
			
			
			
			
			
			
		
	
				
		


			private function cerrar_conexion ($texto = null, $kilobytes = 64) {				
	
				ignore_user_abort(true);
				//ob_end_clean();
				ob_start();
	
				if ($texto !== null)
					echo $texto;
				
		
				//$serverProtocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL', FILTER_SANITIZE_STRING);
				//header($serverProtocol . ' 200 OK');				
				//header('Content-Encoding: none');  // Disable compression (in case content length is compressed).
				//header("HTTP/1.1 200 OK"); 
				//header("Status: 200 All rosy");

				header('Content-Type: application/json; charset=utf-8');				
				header('Content-Length: ' . ob_get_length());
				header('Connection: close');  // Close the connection.
								
				http_response_code(200); 
												
				ob_implicit_flush(true);
				ob_end_flush();
				ob_flush();
				flush();
				
				
				/*
					luego de emitir el mensaje se debe llenar el buffer
					65536 bytes (64 kb) 					
					y se desborda con  1 byte 
				*/
		
				
				echo ' '; // 1 byte

				// Emite directamente los 64 kb
				// echo(str_repeat(' ', 1042 * $kilobytes)); // 64 kb

				// Emite de a 1 kilobyte
				// A la espera de que el buffer se desborde antes de los 64 kb y corte la conexión
				for ($x = 0; $x < $kilobytes; $x++) { 					
					echo str_repeat('                ', 64); // 1 kb			
					ob_flush();
					flush();                            
				}
				
			}

				
		
			
			//  --------------------------------------------------------









			// ------ CONEXION -------------------
				
			// función que realiza la conexión con el robot
			
			private function conectar ($datos= array()) {
				if ($this->robot_url == null) {
					error_log("No hay URL o CLAVE definida \n");
					return false;
				}
				
				$respuesta = $this->Conectar_url ($this->robot_url, $datos);
				
				$arregloresultado =  json_decode($respuesta, true);								
				
				$mensaje_servidor = $this->nuevo_mensaje ();
				$mensaje_servidor->arreglo($arregloresultado);
				
				
				return $arregloresultado;
			}
			
			
			
			// conexión genérica
			private function conectar_url ($url, $parametros = array ()) {				
				
				// prepara la conexión
				$handle = curl_init($url);
				curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 25);
				curl_setopt($handle, CURLOPT_TIMEOUT, 60);
				curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($parametros));
				curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8"));
				
				
				// realiza la conexión		
				$respuesta = curl_exec ($handle);
				
				curl_close($handle);
				return $respuesta;
			}








				
			
			
			public function enviar ($mensaje) {					
				// El Historial se guarda en en método de envíar_api y webhook.
				
				// envía por webhook
				if ($this->webhook_enviado == false)
					if ($this->webhook)
						return $this->webhook($mensaje);
				
				// envía por api
				//return $this->conectar($mensaje->arreglo());
				return $this->enviar_api($mensaje);

			}
			
			
			public function enviar_api ($mensaje) {
				$this->historial [] = $mensaje->arreglo();
				if ($this->no_enviar)
					return;
				return $this->conectar($mensaje->arreglo());
			}
		


			public function webhook ($mensaje) {				
				$this->historial [] = $mensaje->arreglo();
				if ($this->no_enviar)
					return;
				$this->webhook_enviado = true;
				$json = json_encode($mensaje);
				$this -> cerrar_conexion ($json);
			}

	

			
			
			
			
			
			
			
			
			
			
			

			
			
			
			
			
			
			
			
			
			





			
			
			
			
			
			
			// api
			
			// función que obtiene mensajes nuevos del bot | método api
			private function actualizacion ($actualizacion = null) {
				$parametros['method'] = "getUpdates";				
				if ($actualizacion != null)
					$parametros['offset']  = $actualizacion;
				
				$respuesta = $this->conectar ($parametros);		
				
				
				return $respuesta;
			}
			
			
			
			
		
			
			
			
			
			// --------- Archivo | Esto hay que revisar, se puede mejorar
			// O se puede reemplazar directamente por algo parecido a la función mensaje_repetido (..) 
			
			private $archivo = null;
			private $archivo_nombre = 'contador.txt';

			private function contador ($valor = null) {
				
				if ($valor === null) 
					return $this->leer_contador ();
					
				$this->guardar_contador ($valor + 1);
				
			}
			
			
			private function leer_contador () {
				
				$nombreArchivo = $this->archivo_nombre;
				
				if (file_exists($nombreArchivo))
					$archivo = fopen($nombreArchivo,'r+');
				else
					$archivo = fopen($nombreArchivo,'w+');
				
				$this->archivo = $archivo;
				
				flock($archivo, LOCK_EX);
				$valor = fgets($archivo);				
				
				return $valor;
			}
			
			
			private function guardar_contador ($valor = 0) {
				
				if ($this->archivo == null)
					$this->leer_contador();
				
				$archivo = $this->archivo;
				
				rewind($archivo);
				fputs($archivo, $valor);
				fclose($archivo);				
			}
			
			
			
			
			

				
			/* 
				 INUNDACIÓN / Repeticiones
				 Previene mensajes repetidos (mismo update_id)
				
			*/
			public function mensaje_repetido ($mensaje, $nombreArchivo = 'actualizacion.txt') {


				
				$id_recibido = $mensaje->update_id;

				
				
				// abre el archivo
				if (file_exists($nombreArchivo))
					$archivo = fopen($nombreArchivo,'r+');
				else
					$archivo = fopen($nombreArchivo,'w+');
								
				
				// lee el archivo
				flock($archivo, LOCK_EX);
				$valor = fgets($archivo);

				

				// guarda el archivo
				rewind($archivo);
				fputs($archivo, $id_recibido);

				// cierra el archivo
				fclose($archivo);				

				return $valor === $id_recibido;
			}
			
			
			
			
			
			
			

			/*
				 Envío de Historial
				 arreglo de mensajes.
				 Es para que un interprete externo envíe los mensajes (Robot intermedio SSL)

				 NO ENVÍA EL HISTORIAL - muestra un arreglo de "envíos".
			*/

			public function enviar_historial () {
				
				$historial = array_map (
						function ($x) { 
							$x ['robot'] = $this->robot_clave; 
							if (isset($x['recibir_respuesta']) == false)
								$x['recibir_respuesta'] = $this->recibir_respuesta;
							return $x ; 
						}
						, $this->historial );

				header('Content-Type: application/json; charset=utf-8');
				
				echo json_encode ($historial);				
			}










			
			
		
		public function respuesta ($mensaje_origen = null) {
			$mensaje = new MensajeRobot ($this);
			
			if ($mensaje_origen == null)
				return $mensaje;
						
			$mensaje->destinatario ($mensaje_origen->sala, $mensaje_origen->mensaje_id);
			
			return $mensaje;
		}
		
		
		
		
			// Impresiones --------------
		
		
			
			private function imprimir_prueba ($contenido, $titulo = 'RoBOT') {
				if ($this->modo_prueba)
					$this->imprimir ($contenido, $titulo);
			}
		
		
			private $titulo_impreso = false;
			private function imprimir ($contenido, $titulo = 'RoBOT') {
				if ($this->titulo_impreso == false)
					echo "<h1 style = 'background-color: black; color: white; margin: 0px'> 
							$titulo 
						</h1>" . PHP_EOL;
				echo '<pre style = "font-size: large;">'. PHP_EOL;
				print_r ($contenido);
				echo '</pre>'. PHP_EOL;
				@ob_flush ();
				@flush();
				$this->titulo_impreso = true;
			}
			
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	














	/*

			Clase de Mensajes

		Esta clase es la que contiene código de la API de Telegram.
		Sirve para enviar mensajes a la API.
		También sirve para recibir las respuesta de la API.

	*/


	
	
	
	
	class MensajeRobot extends Magica  {
		
			// objeto robot
			// se usa para facilitar el envío usando esta clase y no la del robot.
			public $robot = null;
			public $exitoso = false;
			
			// Esto es una idea para comprimir el método "simplificar"
			private const FANTASIA = [
					'destinatario' => 'chat_id',
					'texto'   => 'text',
					'latitud' => 'latitude',
					'longitud'=> 'longitude',
					'metodo'  => 'method', 
					'silencioso' => 'disable_notification',
					'sin_previsualizacion' => 'disable_web_page_preview'
					
					];
				
			
			
			
			public function __construct (Robot $robot = null) {
				$this->robot = $robot;
				
				$this->method = 'getUpdates';
				$this->parse_mode = 'html';
				
			}
			
			public function enviar () {
				$this->robot->enviar($this);
			}
			
			public function enviar_api () {
				$this->robot->enviar_api($this);
			}
			
			
			
			
			
			
			/*
			
			// sobrescribe métodos mágicos 
			
			public function valor ($clave, $valor) {
				if ($clave == 'texto') {
						$this->elementos ['method'] = 'sendMessage';
						$clave = 'text'	;
					}
				
				$this->elementos [$clave] = $valor;
			}
			
			
			public function obtener  ($clave) {
				if ($clave == 'texto') 
					$clave = 'text';										
				return array_key_exists ($clave, $this->elementos) ? $this->elementos [$clave] : null;
			}
			
			
			*/
			
			
			
			
			
			public function exitoso () {
				return $this->exitoso;
			}
			
			
			// Al recibir mensajes se simplifica los datos para un acceso comprensible.
			// Debe quedar público porque lo usa ROBOT
			public function simplificar () {
				$mensaje = $this->elementos;
				$this->elementos ['texto']      = '';
				$this->elementos ['mensaje_id'] = '';
				$this->elementos ['comando_boton'] = '';
				$this->elementos ['alias']      = '';
								
				
					
				if (isset ($mensaje ['message'] ['from']['username']))
					$this->elementos ['alias']      = $mensaje ['message'] ['from']['username']; // alias @usuario					
				
				if (isset ($mensaje ['message'])) {
					$this->elementos ['mensaje_id'] = $mensaje ['message'] ['message_id']; // usado para señalar el mensaje y responder
					$this->elementos ['sala']       = $mensaje ['message'] ['chat']['id']; // la ventana de conversación
					$this->elementos ['usuario_id'] = $mensaje ['message'] ['from']['id']; // identificador de usuario.
					
					$this->elementos ['nombre']     = filter_var ($mensaje ['message'] ['from']['first_name'], FILTER_SANITIZE_STRING);					
					$this->elementos ['tipo']       = $mensaje ['message'] ['chat']['type'];
					
				}
				
				if (isset($mensaje ['message'] ['text']))
					$this->elementos ['texto']  = filter_var ($mensaje ['message'] ['text'], FILTER_SANITIZE_STRING);
				
				if (isset ($mensaje ['message'] ['chat']['title']))				
					$this->elementos ['titulo'] = $mensaje ['message'] ['chat']['title'];
			
			
				$this->elementos ['entrada']    = (isset ($mensaje ['message']  ['new_chat_member']));
				$this->elementos ['salida']     = (isset ($mensaje ['message']  ['left_chat_member']));
				
				
				$this->elementos ['es_boton'] = false;
				
				if (isset ($mensaje ['callback_query'])) {
					$this->elementos ['es_boton'] = true;
					$this->elementos ['texto'] = $mensaje ['callback_query']['data'];
					$this->elementos ['comando_boton'] = $mensaje ['callback_query']['data'];
					$this->elementos ['usuario_id'] = $mensaje ['callback_query'] ['from']['id']; // identificador de usuario.
					
					$this->elementos ['nombre']     = filter_var ($mensaje ['callback_query'] ['from']['first_name'], FILTER_SANITIZE_STRING);					
					$this->elementos ['tipo']       = $mensaje ['callback_query'] ['message'] ['chat']['type'];
					$this->elementos ['sala']       = $mensaje ['callback_query'] ['message'] ['chat']['id']; // la ventana de conversación
					$this->elementos ['mensaje_id'] = $mensaje ['callback_query'] ['message'] ['message_id']; // usado para señalar el mensaje y responder										

					if (isset ($mensaje ['callback_query'] ['from']['username']))
						$this->elementos ['alias']      = $mensaje ['callback_query'] ['from']['username']; // alias @usuario
				}
				

				
				
				if (isset ($this->elementos ['tipo'])) {
					$this->elementos ['es_grupo']   = ($this->elementos ['tipo'] == 'group' || $this->elementos ['tipo']  == 'supergroup');
					$this->elementos ['es_canal']   = ($this->elementos ['tipo'] == 'channel');
					$this->elementos ['es_privado'] = ($this->elementos ['tipo'] == 'private');
				}
				
				
				// comandos 
				$this->elementos ['comando']   = null;
				$this->elementos ['parametro'] = null;
				
				$texto = $this->elementos ['texto'];
				if (substr ($texto, 0,1) == '/') {
					
					$comando = explode (' ', $texto) [0] ;
					$parametro =  str_replace ($comando . ' ', '', $texto);
					if ($parametro == $comando)
						$parametro = '';
					
					$this->elementos ['comando'] = $comando;
					$this->elementos ['parametro'] = $parametro;
					
				}

				$this->elementos ['enlaces'] = $this->enlaces ($texto);
				$this->elementos ['etiquetas'] = $this->etiquetas ($texto);				
				

				if (isset ($mensaje ['ok']))
					$this->exitoso = $mensaje ['ok'];
				else
					return $this;
	



					
				
				return $this;
			}
				 
				 

			// Obtiene los enlaces a partir del $texto
			public function enlaces ($texto) {		
								
				
				$expresion = '~[a-z]+://\S+~';				
				
				if(preg_match_all($expresion, $texto, $enlaces))				
					//return $enlaces[0];
					return array_map (function ($e) {
						return rtrim ($e, '/');	
					}, $enlaces[0] );
					
				return array ();
			}
			
			
			// Obtiene etiquetas a partir del $texto
			public function etiquetas ($texto) {
				
				$expresion = '~#+\S+~';
				
				if(preg_match_all($expresion, $texto, $etiquetas))					
					return array_map (function ($e) { return str_ireplace ('#', '', $e); } , $etiquetas[0]);
								
				return array ();
				
			}
			
			


			

			
			
			
			
			
			
			
			
			
			
			
			
			

			// ------ ENVIO DE MENSAJES (compatible con wh y api) -------------------	
			
			public function enviar_texto ($destinatario, $mensaje, $id_respuesta= null) {
				$this->destinatario ($destinatario, $id_respuesta);
				$this->texto ($mensaje);				
				return $this->enviar ();
			}
			
			public function enviar_coordenada ($destinatario, $latitud, $longitud, $id_respuesta=null) {
				$this->destinatario ($destinatario, $id_respuesta);
				$this->coordenada   ($latitud, $longitud);				
				return $this->enviar ();
			}
			
			public function enviar_imagen ($destinatario, $imagen, $texto = null, $id_respuesta= null) {
				$this->destinatario ($destinatario, $id_respuesta);
				$this->imagen ($imagen, $texto);	
				return $this->enviar ();
			}

			public function enviar_video($destinatario, $video, $texto = null, $id_respuesta= null) {
				$this->destinatario ($destinatario, $id_respuesta);
				$this->video ($video, $texto);	
				return $this->enviar ();
			}
			
			public function enviar_accion ($destinatario, $accion = null) {
				$this->destinatario ($destinatario);
				$this->accion ($accion);
				return $this->enviar ();
			}
			
			public function enviar_dato ($destinatario, $comando, $parametros = array ()) {
				$this->destinatario ($destinatario);
				$this->comando ($comando);
				foreach ($parametros as $parametro => $valor)
					$this->parametros[$parametro] = $valor;
				
				return $this->enviar ();
			}
			
			
			public function enviar_objeto ($destinatario, $objeto, $id_respuesta = null) {
				
			}
			
			
			
			
			
				
			
			public function editar_imagen ($destinatario, $id, $imagen, $texto = null) {
				$this->destinatario ($destinatario);
				$this->media ($imagen, 'photo' , $id, $texto);
				return $this->enviar ();
			}
			
			
			
			
			public function editar_texto ($destinatario, $mensaje, $id_mensaje = null) {
				$this->destinatario ($destinatario);
				$this->edicion ($mensaje, $id_mensaje);

				return $this->enviar ();
			}
		
			
			
			


			public function salir ($chat_id) {				
				$this->elementos ['method'] = "leaveChat";
				$this->elementos ['chat_id'] = $chat_id;
				return $this->enviar ();
			}
			
			
			
			
			// ---------- prepara el arreglo de envíos ---------------------------------	
			
			// usuario destinatario
			public function destinatario ($chat_id, $id_mensaje = '') {
				$this->elementos['chat_id'] = $chat_id;
				$this->respuesta ($id_mensaje);
			}
			
			// mensajes de texto
			public function texto ($mensaje) {
				$this->elementos['method'] = 'sendMessage';
				$this->elementos['text']   = $mensaje;
			}
			
			// respuesta a mensajes
			public function respuesta ($id_mensaje = '') {
				$this->elementos ['reply_to_message_id']=$id_mensaje;
			}
			
			// edición de mensajes
			public function edicion ($texto, $id_mensaje = '') {				
				$this->elementos['method'] = 'editMessageText';
				$this->elementos['text']   = $texto;
				if ($id_mensaje != null)
					$this->elementos['message_id']   = $id_mensaje;
			}

			// coordenadas
			public function coordenada ($latitud, $longitud, $segundos = 0) {
				$this->elementos['method'] = 'sendLocation';
				$this->elementos['latitude']  = $latitud;
				$this->elementos['longitude'] = $longitud;
				
				if ($segundos >= 60)
					$this->elementos['live_period'] = $segundos;
				if ($segundos >= 86400)
					$this->elementos['live_period'] = 86400;
			}
			
			
			public function accion ($accion = 'upload_photo') {
				
				switch ($accion) {
					case null:
					case '':
					case 'subiendo fotografia': $accion = 'upload_photo'; break;
					case 'escribiendo': $accion = 'typing'; break;
					case 'grabando video': $accion = 'record_video'; break;
					case 'subiendo video': $accion = 'upload_video'; break;
					case 'grabando audio': $accion = 'record_audio'; break;
					case 'subiendo audio': $accion = 'upload_audio'; break;
					case 'subiendo documento': $accion = 'upload_document'; break;
					case 'ubicacion': $accion = 'find_location'; break;
					case 'grabando video mensaje': $accion = 'record_video_note'; break;
					case 'subiendo video mensaje': $accion = 'upload_video_note'; break;
				}
				
				$this->elementos['method'] = 'sendChatAction';
				$this->elementos['action'] = $accion;
				
			}
			
			
			
			// botones 
			public function teclas (...$teclas) {				
				$this->elementos ['reply_markup']['keyboard'] = array($teclas);
				$this->elementos ['reply_markup']['one_time_keyboard'] = true;
				$this->elementos ['reply_markup']['resize_keyboard']   = true;
			}
			
	
	
			public function boton ($texto, $comando = null) {
				if ($comando == null)
					$comando = $texto;
				
				$this->elementos ['reply_markup']['one_time_keyboard'] = true;
				$this->elementos ['reply_markup']['resize_keyboard']   = true;
				$this->elementos ['reply_markup']['inline_keyboard'][] = 
				[[	'text' => $texto, 'callback_data' => $comando ]];
			}
			
			
			
			public function botones ($botones = array()) {
				
				$this->elementos ['reply_markup']['resize_keyboard']   = true;
				
				$botonera = array ();
								
				
				foreach ($botones as $texto => $comando)
					$botonera [] = 	['text' => $texto, 'callback_data' => $comando ];
				
				$this->elementos ['reply_markup']['inline_keyboard'][] = $botonera;
			}
			
			
			/*public function botones (...$botones) {
				
				$this->elementos ['reply_markup']['resize_keyboard']   = true;
				
				$botonera = array ();
								
				foreach ($botones as $boton)
				foreach ($boton as $texto => $comando)
					$botonera [] = 	['text' => $texto, 'callback_data' => $comando ];
				
				$this->elementos ['reply_markup']['inline_keyboard'][] = $botonera;
			}*/
			
			
			



			// Opciones --------------------------
			
			public function silencioso ($valor = true) {				
				$this->elementos['disable_notification']=$valor;
			}
			
			public function sin_previa ($valor = true) {
				$this->elementos['disable_web_page_preview']=$valor;
			}
			
			// Markdown MarkdownV2 html
			public function formato ($formato = 'html') {
				$this->elementos['parse_mode'] = $formato;
			}
						
			
			// otros datos
			public function metodo ($comando) {
				$this->elementos['method'] = $comando;
			}
			


			
			public function atributo ($clave, $valor) {
				$this->elementos [$clave] = $valor;
			}
			








			// Métodos que no se pueden usar por webhook (requieren respuestas, no son respuestas)

			public function obtener_ ($chat_id, $metodo) {
				$this->elementos ['method']  = $metodo;
				$this->elementos ['chat_id'] = $chat_id;				
				return $this->enviar_api ();
			}

			public function obtener_informacion ($chat_id) {
				return $this->obtener_($chat_id, 'getChat');
			}

			public function obtener_administradores ($chat_id) {
				return $this->obtener_($chat_id, 'getChatAdministrators');
			}

			public function obtener_cantidad_usuarios ($chat_id) {
				return $this->obtener_($chat_id, 'getChatMembersCount');
			}

			
			
			



			// Devuelve los datos del mensaje en forma de arreglo
			// O convierte incorpora un objeto mensaje a sus datos
			public function arreglo ($objeto = null) {
				if ($objeto == null)
					return $this->elementos;
				else
					$this->agregar((array) $objeto);
			}
			
			
			
			
			
			
			
			
			// archivos ---
			
			// fotos -
			
			public function imagen ($imagen, $texto = null) {
				$this->elementos['method'] = 'sendPhoto';
				$this->elementos['photo']  = $imagen;				
				if ($texto != null)
					$this->elementos['caption'] = $texto;				
			}
			
			public function video ($video, $texto = null) {
				$this->elementos['method'] = 'sendVideo';
				$this->elementos['video']  = $video;
				if ($texto != null)
					$this->elementos['caption'] = $texto;				
			}
			
			
			public function media ($objeto, $media = 'photo', $id = null, $texto = null) {
				$this->elementos['method'] = 'editMessageMedia';
				if ($id != null)
					$this->elementos['message_id'] = $id;				
				$this->elementos['media']['type']  = $media;
				$this->elementos['media']['media']  = $objeto;
				//$this->elementos['media']['parse_mode']  = 'html';
				$this->elementos['media']['caption'] = '';
				if ($texto !== null)
					$this->elementos['media']['caption'] = $texto;
			}
			
			/*
			public function zmedia ($objeto, $media = 'photo', $texto = null) {
				$this->elementos['method'] = 'editMessageMedia';
				$this->elementos['media'] 
				= '{
					"type": "' . $media. '"
				}';
				
			}
			*/
			
			
			
			
			

		// --- otros -----

			public function recibir_respuesta ($valor = true) {
				$this->elementos['recibir_respuesta']  = $valor;
			}
			
			
			public function fantasia ($clave) {
				
			}
			
			
			
			
			// indices profundos.
			// $x ['a'] ['b'] ['c'] = 'z';			
			// print_r (indice ($x, 'a', 'b', 'c'));

			private function indice ($arreglo = array (), ... $claves) {
				$recorrido = $arreglo;
				foreach ($claves as $clave)
					if (array_key_exists ($clave, $recorrido))
						$recorrido = $recorrido [$clave];
					
				return $recorrido;	
			}
			
			
			
			
			
			
			
			
			
			
			
			

			
			
			
	}



		
?>
