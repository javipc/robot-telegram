# RoBot Telegram


Ofrece métodos sencillos para el manejo de robots de Telegram.

Esta nueva versión ofrece una mejora en la abstracción.

Por un lado la clase Robot, que posee la información de configuración y métodos de envío.

Por otro lado la clase de mensajes, que posee todos los detalles de los mensajes recibidos y mensajes para enviar.




## Métodos sencillos:

enviarMensaje ($destinatario, $mensaje, $id_respuesta= null)
enviarCoordenada ($destinatario, $latitud, $longitud, $id_respuesta=null) 
enviarImagen ($destinatario, $imagen, $texto = null, $id_respuesta= null) 			
enviarDato ($destinatario, $comando, $parametros = array ())



## Crear el robot
	
	require ('robot.php');
	
	// clave del robot
	// se puede configurar con una constante o con el método "clave"
	const robot_clave = '5044:AASA_ojAo';
		
	// crea el robot
	$robot = new robot ();
	
	// si la clave del robot está definida en la constante robot_clave
	// esta línea no es necesaria, el robot la toma automáticamente de la constante.
	$robot->clave ('5044:AASA_ojAo');
	
	
## Configuración WebHook
	
	// configura webhook
	$robot->url ('https://seguime.000webhostapp.com/robotprueba.php');	
	
	// muestra la información del robot
	$robot->info ();	
	
	// elimina webhook
	$robot->borrarurl ();	
	


## Envío de mensajes

	// recibe los datos del servidor
	$peticion = $robot->peticion ();
	
	// obtiene un objeto "mensaje" para responder.	
	// el mensaje ya contiene el destinatario y el id del mensaje al que responderá
	$mensaje = $robot->mensaje ();
	
	// texto del mensaje
	$mensaje->texto = '¡Hola!';
	
	// envía el saludo.
	$mensaje->enviar ();
	
	
	
	
## Comandos del RoBot
	
			

	if ($peticion->comando != null)
		switch ($peticion->comando) {
			case '/hola': 
				$mensaje-> texto = '¿Qué tal?';
				$mensaje-> enviar ();
				break;
				
			case '/soy':
				$nombre = $peticion->parametro;
				$mensaje-> texto = 'Hola ' . $nombre;
				$mensaje-> enviar ();
			
			default :
				$mensaje-> texto = 'No entendí';
				$mensaje-> enviar ();
		}
	
	
	
	
	// Otra forma de enviar mensajes.
	$mensaje-> enviarmensaje (
		$peticion->sala,
		'El usuario ' . $peticion->nombre . ' dijo: ' . $peticion->texto );
	
	





# Proyectos en los que se utiliza este programa

Seguime 
https://t.me/seguimebot 

Kawaii Tokei
https://t.me/relojbot

Listagrama
https://t.me/listagramabot

				
			


# Más aplicaciones

Al no tener publicidad este proyecto se mantiene únicamente con donaciones.
Siguiendo este enlace tendrás más información y también más aplicaciones.
[Más información](https://gitlab.com/javipc/mas) 



